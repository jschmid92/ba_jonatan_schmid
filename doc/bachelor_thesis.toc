\contentsline {chapter}{\numberline {1}Background}{3}
\contentsline {section}{\numberline {1.1}Safety-related Software}{3}
\contentsline {section}{\numberline {1.2}Software Process}{4}
\contentsline {section}{\numberline {1.3}MISRA Coding Guidelines}{7}
\contentsline {chapter}{\numberline {2}Development Methods}{9}
\contentsline {section}{\numberline {2.1}Model-Based Design}{9}
\contentsline {section}{\numberline {2.2}Component-Based Design}{10}
\contentsline {section}{\numberline {2.3}Continuous Integration}{12}
\contentsline {chapter}{\numberline {3}Involved Tools}{13}
\contentsline {section}{\numberline {3.1}Subversion}{13}
\contentsline {section}{\numberline {3.2}ASCENT}{17}
\contentsline {section}{\numberline {3.3}QAC}{18}
\contentsline {chapter}{\numberline {4}Analysis}{19}
\contentsline {section}{\numberline {4.1}Current Projects}{19}
\contentsline {section}{\numberline {4.2}Software developer workflow}{20}
\contentsline {subsection}{\numberline {4.2.1}Working Environment Preparation}{21}
\contentsline {subsection}{\numberline {4.2.2}Quality QAC Reports}{24}
\contentsline {chapter}{\numberline {5}Application: QAC Reports}{26}
\contentsline {section}{\numberline {5.1}Requirements}{26}
\contentsline {section}{\numberline {5.2}Design}{26}
\contentsline {subsection}{\numberline {5.2.1}Data Producer}{28}
\contentsline {subsection}{\numberline {5.2.2}SQL Storage}{31}
\contentsline {subsection}{\numberline {5.2.3}Presenter}{33}
\contentsline {subsection}{\numberline {5.2.4}SQL Renderer}{33}
\contentsline {section}{\numberline {5.3}Implementation}{34}
\contentsline {subsection}{\numberline {5.3.1}SQL Table Population}{34}
\contentsline {chapter}{\numberline {6}Application: Working Environment}{35}
\contentsline {section}{\numberline {6.1}Requirements}{35}
\contentsline {section}{\numberline {6.2}Design}{35}
\contentsline {section}{\numberline {6.3}Implementation}{35}
\contentsline {chapter}{\numberline {7}Conclusion}{36}
\contentsline {chapter}{\numberline {8}Appendices}{37}
